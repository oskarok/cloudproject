package handlers

import (
	"encoding/json"
	"lastfm/utils"
	"net/http"
	"strings"
	"time"
)

//StartTime -
var StartTime time.Time

//getStatus -
func getStatus(w http.ResponseWriter, url string) int {

	result, err := http.Get(url)
	if err != nil {
		http.Error(w, "Could not retrieve from server", http.StatusBadRequest)
	}
	return result.StatusCode
}

//StatusHandler -
func StatusHandler(w http.ResponseWriter, r *http.Request) {

	parts := strings.Split(r.URL.Path, "/")
	version := parts[2]

	//lastfmUrl := "http://ws.audioscrobbler.com/2.0"

	lastfmURL := "http://www.last.fm/api/"

	mouritsURL := "https://mourits-lyrics.p.rapidapi.com/"

	var stat utils.Status

	//Database should run so 200 by default
	stat.DB = 200

	// Creates a request for permission to access information from mourits lyricsD
	req, _ := http.NewRequest("GET", mouritsURL, nil)
	// /req, _ := http.NewRequest("GET", lyricsURL, nil)

	// host and api permission and key
	req.Header.Add("x-rapidapi-key", "4de6d276aemshf0d4b72ac1abcbdp1b154bjsn9c5795afc216")

	// gets the response from server
	res, _ := http.DefaultClient.Do(req)

	//Retrieves the status code for Mourits API, should return 200
	stat.Mourits = res.StatusCode

	//Retrieve the status code for LastFM API, should return 200
	stat.Lastfm = getStatus(w, lastfmURL)

	//Retrieves version number for the api
	stat.Version = version

	//API service's uptime since running the code, in seconds
	stat.Uptime = float64(time.Since(StartTime).Seconds())

	//Time stamp for the service
	stat.Time = time.Now()

	w.Header().Add("content-type", "application/json")
	err := json.NewEncoder(w).Encode(stat)
	if err != nil {
		http.Error(w, "Could not encode", http.StatusBadRequest)
	}

	defer res.Body.Close()

}
