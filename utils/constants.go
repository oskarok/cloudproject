package utils

//APIURL - Root url for lastfm api
const APIURL = "http://ws.audioscrobbler.com/2.0/?method="

//LYRICURL -root url for lyrics api
const LYRICURL = "https://mourits-lyrics.p.rapidapi.com/?"

//APIKEY - key to use lastfm apik
const APIKEY = "7f39fb92f00358e621eddf7368040df3"

//ProjectID - Name of database project
const ProjectID = "lastapi-804fb"

//CollectionWebhooks - Name of collection webhook
const CollectionWebhooks = "webhook"

//CollectionToptracks - Name of collection toptrack
const CollectionToptracks = "toptrack"

//DbWh - Connection to firebase collection Webhooks
var DbWh FirestoreDatabase

//DbTt - Connection to firebase collection TopTrack
var DbTt FirestoreDatabase
