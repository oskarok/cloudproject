## Project description

This project lets you use the lastfm api to get information about music and artists. You can retrieve info on artists, tracks, genres, as well as the top tracks and artists in a specific country or genre. 
You can also get the lyrics of a specific track using this api: https://rapidapi.com/PlanetTeamSpeak/api/mourits-lyrics. We used webhooks to and firebase to keep track of the top five tracks on lastfm.

Because we used firebase if you want to run the project locally you will need your own firebase key. 
Put that in the Init() function in the database file in line 16. 

OpenStack URL : http://10.212.139.166:8080/lastfm/v1/

Run it locally with : http://localhost:8080/lastfm/v1/


## Endpoints

### Music
Gets info on many different parts of the lastfm api. 
During the project we decided to put all the similar functions into one big one instead, which handles basically all get requests from the api. 

#### Usage 

http://10.212.139.166:8080/lastfm/v1/music/[tag|tag_top_artist|tag_top|tag_top_tracks|track|artist|artist_top_country|track_top_country]

tag: gets info about a specific tag/genre.
http://10.212.139.166:8080/lastfm/v1/music/tag?tag=rock

tag_top_artists: gets the top artists for a specific tag/genre.
http://10.212.139.166:8080/lastfm/v1/music/tag_top_artist?tag=rock&limit=

tag_top_tracks: gets the top tracks for a specific tag/genre.
http://10.212.139.166:8080/lastfm/v1/music/tag_top_tracks?tag=rock&limit=

tag_top: gets the most popular tags/genres on last.fm.
http://10.212.139.166:8080/lastfm/v1/music/tag_top?tag=rock&limit=

track: gets info about a specific track from a specific artist, includes lyrics from a different api.
http://10.212.139.166:8080/lastfm/v1/music/track?artist=cher&track=believe

artist: gets info about a specific artist.
http://10.212.139.166:8080/lastfm/v1/music/artist?artist=cher

artist_top_country: gets the top artists in a specific country.
http://10.212.139.166:8080/lastfm/v1/music/artist_top_country?country=spain&limit=

track_top_country: gets the top tracks in a specific country.
http://10.212.139.166:8080/lastfm/v1/music/track_top_country?country=spain&limit=

### Status
Gets status codes of the APIs and the database as well as uptime and timstamp of when status endpoint was called

http://10.212.139.166:8080/lastfm/v1/status


### Webhooks

http://10.212.139.166:8080/lastfm/v1/webhooks  
Accepts GET request with empty payload  
Gets all webhooks from database.  

Accepts POST request with payload  
Creates new webhook, saving to the database and returns ID.  
JSON payload:  
Event: "NewTop5Track"  
URL: "URL to be invoked"  

http://10.212.139.166:8080/lastfm/v1/webhooks/{id}  
Accepts GET request with empty payload  
Gets webhook with that ID.  

Accepts DELETE request with empty payload  
Deletes webhook from database with that ID.  

Invocation of webhooks:  
There is currently only one webhook feature called "NewTop5Track".  
If you register a webhook with this event your webhook URL will be invoked everytime there is  
a new song that enters the world top 5 tracks chart. The payload that comes with the invocation  
is the time of invocation, name of song, name of artist and the song's listerners count.  

## Original plan
Our original plan was to have many different endpoints and files for the different features of the lastfm api. We also thought about having playlists or favourites option using webhooks. 
We did not have any plans to get the lyrics of a specific track, but we had initially planned to have a way to search for a word of a song, and recieve songtitles that contained that specific word. 

## What we ended up with
After working on the project for about a week we decided to put all the basic handlers and endpoints into one "superhandler". 
This was because we discovered that many of the handlers had mostly the same code, the only major difference
being the url, and parameters like artist, track, etc. This was hard to start with but eventually worked out and we think the project is better because of this. 

For the track endpoint we decided to also have the lyric of the song, which was not availible on the lastfm api, so we had to use a different api for this. 

We decided not to add playlists or favourites in favour of keeping track of the top five tracks on the website and updating the webhooks if a new song enters the top five. 

## Reflection of what went well
* We used different modules from the start of the project, so we did not have any last minute problems with this
* We started working early, and planned what each members tasks were going to be
* Worked well as a group 
* Planning and organizing bigger projects

## Reflection on the hard aspects 
* After the first week we had a lot of work done, but decided to make one big handler instead of many smaller ones. This turned into a bigger challenge than we thought it would be. 
* When the initial tasks were done some group members found themselves with not much to do, so we had to figure out new tasks.
* For the track endpoint it was hard to get data from both the lastfm api for basic info, and the lyrics api and put them both together in one endpoint.

## The new things we learned
* Learned how to use docker
* Plan and organize a project from scratch
* We got better at using git, adding meaningful messages to the commits. 
* Sending structs as parameters in a function 
* Gotten better at using modules and packages

## Total work hours

We spent approximately 100 hours on the project 